import csv
import argparse
import logging.config
import json

import flask
from flask_restful import Resource, Api
from flask import request

logging.config.fileConfig('./logging.conf')


app = flask.Flask(__name__)
api_obj = Api(app)
app.config["DEBUG"] = True

data_stored = {}

SUCCESS = 200
INTERNAL_ERROR = 500

class Data(Resource):
    def get(self):
        """
        Return all the data stored in this node
        :return:
        """
        return data_stored

    def post(self):
        """
        Store all the data in request
        :return:
        """
        try:
            data = json.loads(request.get_data().decode())
            key = list(data.keys())[0]
            value = list(data.values())[0]
            data_stored[key] = value
            return "Data Stored, Total Data present in node: {}".format(len(data_stored)), SUCCESS
        except Exception as e:
            logging.error(e)
            return "", INTERNAL_ERROR

api_obj.add_resource(Data, '/api/v1/entries')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(usage="%(prog)s --port <int>", description="Start the api with the chosen name")
    parser.add_argument('--port', type=int)
    args = parser.parse_args()
    if args.port:
        app.run(port=int(args.port))
    else:
        logging.warning("No port specified")
        parser.print_help()
