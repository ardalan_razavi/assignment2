import requests
from hashlib import sha256
from time import time
import logging.config
import json
from collections import defaultdict
from pprint import pprint

from csv_reader import read_csv


servers = ['http://localhost:5000/api/v1/entries',
           'http://localhost:5002/api/v1/entries',
           'http://localhost:5003/api/v1/entries']
logging.config.fileConfig('./logging.conf')

if __name__ == "__main__":
  # hash(Year:Cause Name:State) => xxxx
  start_time = time()
  counter = 0
  servers_data_loaded = defaultdict(int)
  for r in read_csv():
    try:
      key = hash(':'.join([r[0], r[2], r[3]]))
      server_id = key%len(servers)
      resp = requests.post(servers[server_id], data=json.dumps({key : ','.join(r)}))
      logging.info("[{}] Response: {}".format(counter, resp.status_code))
      servers_data_loaded[server_id] += 1
      counter += 1
    except:
      logging.warning("Data reading error")
      break

  for server in servers:
    resp = requests.get(servers[key % 3])
    pprint(resp.json())

  pprint(servers_data_loaded)
  print("Time taken: {}".format(time()-start_time))
