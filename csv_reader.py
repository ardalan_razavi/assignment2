import csv

def read_csv_dict(file_path='./causes-of-death.csv'):
  with open(file_path, newline='') as f:
      reader = csv.DictReader(f)
      header = reader.fieldnames
      print(header)
      for row in reader:
          yield row


def read_csv(file_path='./causes-of-death.csv'):
  with open(file_path, newline='') as f:
      reader = csv.reader(f)
      for row in reader:
          yield row
